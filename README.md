CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Most multivalue field widgets in Drupal core have a "Add another item" button in the end of the list of items. This allows users to add a new item in the end of the list.

However, if there is a list of items is long and if you want to add an item to the start or towards the start, it's very cumbersome to add an item and move it to top.

This module adds new widgets which allows users to add a new item anywhere in the list.

Currently the new widgets are supported for following field types:
1. Text
2. Long text(s)
3. Number
4. Email
5. Entity reference

REQUIREMENTS
------------

 * This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install this module as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

1. Navigate to `Administration > Extend` and enable the module.
2. Navigate to `Structure > Content type > [Content Type] > Manage form display and select the flexible add more widgets.


MAINTAINERS
-----------

 * Dipak Yadav (dipakmdhrm) - https://www.drupal.org/u/dipakmdhrm
