<?php

namespace Drupal\flexible_add_more_widgets\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EmailDefaultWidget as BaseWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'flexible_add_more_email_default' widget.
 *
 * @FieldWidget(
 *   id = "flexible_add_more_email_default",
 *   label = @Translation("Email with 'Flexible add more'"),
 *   field_types = {
 *     "email"
 *   }
 * )
 */
class EmailDefaultWidget extends BaseWidget {

  use StringTranslationTrait;

  /**
   * Special handling to create form elements for multiple values.
   *
   * Handles generic features for multiple fields:
   * - number of widgets.
   * - AHAH-'add below' button.
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $elements = parent::formMultipleElements($items, $form, $form_state);

    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();

    if ($cardinality == FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED && !$form_state->isProgrammed()) {
      $max = $elements['#max_delta'];
      $field_name = $this->fieldDefinition->getName();
      $parents = $form['#parents'];

      for ($delta = 0; $delta <= $max; $delta++) {
        $id_prefix = implode('-', array_merge($parents, [$field_name], [$delta]));

        // Use the same wrapper used by "Add another item" button.
        $wrapper_id = $elements['add_more']['#ajax']['wrapper'];

        // Add anywhere button.
        $elements[$delta]['add_below'] = [
          '#type' => 'image_button',
          '#src' => drupal_get_path('module', 'flexible_add_more_widgets') . '/images/plus.svg',
          '#title' => $this->t('Add'),
          '#weight' => 50,
          '#name' => strtr($id_prefix, '-', '_') . '_add_below',
          '#attributes' => ['class' => ['field-add-below-submit']],
          '#limit_validation_errors' => [array_merge($parents, [$field_name])],
          '#submit' => [[get_class($this), 'addBelowSubmit']],
          '#delta' => $delta,
          '#ajax' => [
            'callback' => [get_class($this), 'addBelowAjax'],
            'wrapper' => $wrapper_id,
            'effect' => 'fade',
          ],
        ];
      }
    }

    return $elements;
  }

  /**
   * Submission handler for the "Add another item" button.
   */
  public static function addBelowSubmit(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go two levels up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2));

    // Increment the items count.
    $field_name = $element['#field_name'];
    $parents = $element['#field_parents'];
    $field_state = static::getWidgetState($parents, $field_name, $form_state);
    $field_state['items_count']++;
    static::setWidgetState($parents, $field_name, $form_state, $field_state);

    // Reset user input values.
    $allUserInputValues = $form_state->getUserInput();
    if (!empty($allUserInputValues[$field_name])) {
      $fieldInputValues = $allUserInputValues[$field_name];
      foreach ($fieldInputValues as $key => $inputValue) {
        // Increase weight of all items that are after the items containing
        // the trigerring button so that we can accomodate the new empty item
        // after it.
        if ($key > $button['#delta']) {
          $inputValue['_weight'] = (string) ((int) $inputValue['_weight'] + 1);
        }
        $newInputValues[] = $inputValue;

        if ($key == $button['#delta']) {
          $newInputValues[] = [
            'target_id' => '',
            '_weight' => (string) ($key + 1),
            'add_below' => '',
          ];
        }
      }
    }
    else {
      $newInputValues = [
        [
          'target_id' => '',
          '_weight' => "0",
          'add_below' => '',
        ],
      ];
    }

    $form_state->setValueForElement($element, $newInputValues);
    NestedArray::setValue($form_state->getUserInput(), $element['#parents'], $newInputValues);

    // Rebuild the form.
    $form_state->setRebuild();
  }

  /**
   * Ajax callback for the "Add another item" button.
   *
   * This returns the new page content to replace the page content made obsolete
   * by the form submission.
   */
  public static function addBelowAjax(array $form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -2));

    // Ensure the widget allows adding additional items.
    if ($element['#cardinality'] != FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
      return;
    }

    // Add a DIV around the delta receiving the Ajax effect.
    $delta = $button['#delta'];
    $element[$delta]['#prefix'] = '<div class="ajax-new-content">' . (isset($element[$delta]['#prefix']) ? $element[$delta]['#prefix'] : '');
    $element[$delta]['#suffix'] = (isset($element[$delta]['#suffix']) ? $element[$delta]['#suffix'] : '') . '</div>';

    return $element;
  }

}
